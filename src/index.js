import React, { useState } from "react";
import ReactDOM from "react-dom";
import MUIDataTable from "mui-datatables";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import IconButton from '@material-ui/core/IconButton';




function App() {
  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("100%");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const columns = ["Pack", "Duración", "Nombre", "Key", "BPM"];
  

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    tableBodyHeight,
    tableBodyMaxHeight,
    print: false,
    viewColumns	: false,
    download: false,
    onRowsDelete: true
  };
  

  const data = [
    
    ["Jaden Collins", "0:10", "Guitarra acustica 2", "F#min", "85"],
    ["Jaden Collins", "0:12", "Guitarra acustica 3", "F#min", "85"],
    ["Jaden Collins", "0:15", "Guitarra acustica 4", "F#min", "85"],
    ["Jaden Collins", "0:85", "Guitarra acustica 5", "F#min", "85"],
    ["Jaden Collins", "0:18", "Guitarra acustica 6", "F#min", "85"],
    ["Jaden Collins", "0:11", "Guitarra acustica 7", "F#min", "85"],
    ["Jaden Collins", "0:17", "Guitarra acustica 8", "F#min", "85"],
    ["Jaden Collins", "0:15", "Guitarra acustica 9", "F#min", "85"],
    ["Jaden Collins", "0:10", "Guitarra acustica 10", "F#min", "85"],
    ["Jaden Collins", "0:20", "Guitarra acustica 11", "F#min", "85"],
    ["Jaden Collins", "0:8", "Guitarra acustica 12", "F#min", "85"],
    ["Jaden Collins", "0:15", "Guitarra acustica 4", "F#min", "85"],
    ["Jaden Collins", "0:85", "Guitarra acustica 5", "F#min", "85"],
    ["Jaden Collins", "0:18", "Guitarra acustica 6", "F#min", "85"]

  ];

  return (
    <React.Fragment>
     
      <MUIDataTable
        title={"Samples"}
        data={data}
        columns={columns}
        options={options}
      />
    </React.Fragment>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
